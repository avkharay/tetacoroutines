package ru.mts.data.news.remote

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import ru.mts.data.news.db.NewsEntity
import ru.mts.data.news.repository.News


class NewsDto {
    @Parcelize
    data class Request(@SerializedName("id") val id: Int) : Parcelable

    @Parcelize
    data class Response(@SerializedName("articles") val articles: List<ArticlesDto>) : Parcelable

    @Parcelize
    data class ArticlesDto(
        @SerializedName("author")
        val author: String,

        @SerializedName("title")
        val title: String
    ) : Parcelable
}

internal fun NewsDto.Response.toDomain(): List<News> {
    return articles.map { News(author = it.author, title = it.title) }
}

internal fun NewsDto.ArticlesDto.toDomain(): News {
    return News(author = this.author, title = this.title)
}

internal fun NewsDto.ArticlesDto.toEntity(): NewsEntity {
    return NewsEntity(author = this.author, title = this.title)
}