package ru.mts.data.news.repository

data class News(

    val author: String? = null,

    val title: String? = null
)
