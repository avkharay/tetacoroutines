package ru.mts.data.news.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import ru.mts.data.news.db.NewsLocalDataSource
import ru.mts.data.news.db.toDomain
import ru.mts.data.news.remote.NewsRemoteDataSource
import ru.mts.data.news.remote.toDomain
import ru.mts.data.news.remote.toEntity
import ru.mts.data.utils.*

class NewsRepository(
    private val newsLocalDataSource: NewsLocalDataSource,
    private val newsRemoteDataSource: NewsRemoteDataSource
) {
    suspend fun getNews(): Flow<Result<NewsResult, Throwable>> {
        return flow {
            newsLocalDataSource.getNews()
                .doOnError {
                    emit(Result.Error(it))
                }
                .doOnSuccess { news ->
                    if (news.isNotEmpty()) {
                        emit(Result.Success(NewsResult(news.map { it.toDomain() }, true)))
                    } else {
                        newsRemoteDataSource.getNews()
                            .doOnError {
                                emit(Result.Error(it))
                            }
                            .doOnSuccess { response ->
                                if (response.articles.isNotEmpty()) {
                                    emit(
                                        Result.Success(
                                            NewsResult(
                                                response.articles.map { articles -> articles.toDomain() },
                                                false
                                            )
                                        )
                                    )
                                    newsLocalDataSource.saveNews(response.articles.map { it.toEntity() })
                                } else {
                                    throw IllegalStateException("empty news")
                                }
                            }
                    }
                }
        }
    }

    suspend fun getRemoteNews(): Flow<Result<NewsResult, Throwable>> {
        return flow {
            newsRemoteDataSource.getNews()
                .doOnError {
                    emit(Result.Error(it))
                }
                .doOnSuccess { response ->
                    if (response.articles.isNotEmpty()) {
                        emit(
                            Result.Success(
                                NewsResult(
                                    response.articles.map { articles -> articles.toDomain() },
                                    false
                                )
                            )
                        )
                        newsLocalDataSource.saveNews(response.articles.map { it.toEntity() })
                    } else {
                        throw IllegalStateException("empty news")
                    }
                }
        }
    }
}

data class NewsResult(val news: List<News>, val localNews: Boolean)
