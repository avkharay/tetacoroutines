package ru.ermolnik.news

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

@Composable
fun NewsScreen(viewModel: NewsViewModel) {
    val state = viewModel.state.collectAsState()
    Box(modifier = Modifier.fillMaxSize()) {
        when (state.value) {
            is NewsState.Loading -> {
                CircularProgressIndicator(
                    modifier = Modifier
                        .size(50.dp)
                        .align(Alignment.Center)
                )
            }
            is NewsState.Error -> {
                Column {
                    Text(text = "Ошибка. Попробуйте еще раз")

                    Button(onClick = { viewModel.updateNews() }) {
                        Text(
                            text = "Обновить",
                            modifier = Modifier
                                .wrapContentSize(),
                            textAlign = TextAlign.Center
                        )
                    }
                }
            }
            is NewsState.Content -> {
                Column {
                    if ((state.value as NewsState.Content).refreshButton) {
                        Button(onClick = { viewModel.updateNews() }) {
                            Text(text = "Обновить новости")
                        }
                    }
                    LazyColumn {
                        /**
                         * При эмите NewsState.Loading почему-то пытается обновится список
                         * и приложение крашится, т.к. не может кастануть state в Content.
                         * До изучения Compose в курсе мы еще не дошли, так что я не понимаю почему это происходит.
                         * Завернул в try catch
                         */
                        try {
                            val news = (state.value as NewsState.Content).news
                            items(news) { item ->
                                Text(
                                    text = item.author ?: "Автор неизвествен",
                                    modifier = Modifier
                                        .wrapContentSize(),
                                    textAlign = TextAlign.Center
                                )
                                Text(
                                    text = item.title ?: "title",
                                    modifier = Modifier
                                        .wrapContentSize(),
                                    textAlign = TextAlign.Center
                                )
                            }
                        } catch (ignore: Exception) {

                        }
                    }
                }
            }
        }
    }
}
